<%
	request.setAttribute("contextName", request.getContextPath());
%>
<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>List Product</h2>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<table class="table table-striped">
					<thead>
						<tr>
							<td></td><td></td><td></td><td></td><td></td><td></td>
							<td><button type="button" id="btn-add"
									class="btn btn-primary pull-left btn-md">Add</button></td>
						</tr>
						<tr>
							<td></td>
							<td><input type="text" id="txt-search"
								class="form-control pull-right" placeholder="Product Code"></td>
							<td><input type="text" id="txt-search1"
								class="form-control pull-right" placeholder="Product Name"></td>
							<td><input type="text" id="txt-search2"
								class="form-control pull-right " placeholder="Description"></td>
							<td>
								<div class="form-group">
									<div class='input-group date' >
										<input type='date' class="form-control" id='datePicker' /> <span
											class="input-group-addon"> <span
											class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</td>
							<td><input type="text" id="txt-search3"
								class="form-control pull-right" placeholder="Created By"></td>
							<td><button type="button" id="btn-search"
									class="btn btn-warning pull-left btn-md">Search</button></td>
						</tr>
						<tr>
							<th>No</th>
							<th>Product Code</th>
							<th>Product Name</th>
							<th>Description</th>
							<th>Created Date</th>
							<th>Created By</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="list-data">
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="modal-form-add" class="modal fade bs-example-modal-md"
	tabindex="-1" role="dialog">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 id="modal-title">Form Product</h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>

<div id="modal-form-edit" class="modal fade bs-example-modal-md"
	tabindex="-1" role="dialog">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content" id="content">
		</div>
	</div>
</div>

<div id="modal-delete" class="modal fade bs-example-modal-sm"
	tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 id="modal-title">Delete Product</h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>

<div id="modal-alert-add" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
			</div> 
			<div class="modal-body">
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                    <p><strong>Data Saved!</strong> New product has been add !</p>
                 </div>
			</div>
		</div>
	</div>
</div>

<div id="modal-alert-edit" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
			</div> 
			<div class="modal-body">
				<div class="alert alert-info alert-dismissible fade in" role="alert">
                    <p><strong>Data Updated!</strong> Data product has been updated !</p>
                 </div>
			</div>
		</div>
	</div>
</div>

<div id="modal-alert-del" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
			</div> 
			<div class="modal-body">
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <p><strong>Data Deleted!</strong> Data product has been deleted !</p>
                 </div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var token = '${_csrf.token}';
	/*
	 * Ajax Setup ini dilakukan agar setiap kali ada 
	 * POST ajax call akan menyertakan header X-CSRF-TOKEN.
	 * Ini perlu dilakukan apabila fitur csrf diaktifkan. Untuk mengetahui
	 * tentang apa itu csrf silakan googling sendiri
	 * 
	 * fitur csrf diaktifkan ada di applicationContext-security.xml line 38
	 */ 
	 function ajaxSetup(token) {
		$.ajaxSetup({
			headers : {
				'X-CSRF-TOKEN' : token
			}
		});
	}
	
	$(document).ready(function() {
		ajaxSetup(token);
	})
	 
	// menampilkan data pertama kali
	function loadData() {
		$.ajax({
			url : '${contextName}/master/product/list',
			dataType : 'html',
			type : 'get',
			success : function(result) {
				$("#list-data").html(result);
			}
		});
	}

	$(document).ready(function() {
		// load data first display
		loadData();
		// ketika btn add di klick
		$("#btn-add").click(function() {
			$.ajax({
				url : '${contextName}/master/product/add',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$("#modal-form-add").find(".modal-body").html(result);
					$("#modal-title").html("Add New Product");
					$("#modal-form-add").removeClass("modal-danger");
					$("#modal-form-add").modal("show");
				}
			});
		});

		// search
		$("#btn-search").click(function() {
			var key = $("#txt-search").val();
			var key1 = $("#txt-search1").val();
			var key2 = $("#txt-search2").val();
			var datePicker = $("#datePicker").val();
			var key3 = $("#txt-search3").val();
			$.ajax({
				url : '${contextName}/master/product/search',
				data : {
					'key' : key, 'key1':key1, 'key2':key2, 'datePicker':datePicker,'key3':key3
				},
				dataType : 'html',
				type : 'get',
				success : function(result) {
					$("#list-data").html(result);
				}
			});
		});
		//
		$("#list-data").on('click', '.btn-view', function() {
			var id = $(this).val();
			$.ajax({
				url : '${contextName}/master/product/view',
				data : {
					'id' : id
				},
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$("#modal-form-edit").find("#content").html(result);
					$("#modal-form-edit").removeClass("modal-danger");
					$("#modal-form-edit").modal("show");
				}
			});
		});
		
		$("#list-data").on('click', '.btn-edit', function() {
			var id = $(this).val();
			$.ajax({
				url : '${contextName}/master/product/edit',
				data : {
					'id' : id
				},
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$("#modal-form-edit").find("#content").html(result);
					$("#modal-form-edit").removeClass("modal-danger");
					$("#modal-form-edit").modal("show");
				}
			});
		});

		$("#list-data").on('click', '.btn-delete', function() {
			var id = $(this).val();
			$.ajax({
				url : '${contextName}/master/product/delete',
				data : {
					'id' : id
				},
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$("#modal-delete").find(".modal-body").html(result);
					$("#modal-title").html("Delete Product");
					$("#modal-delete").addClass("modal-danger");
					$("#modal-delete").modal("show");
				}
				
			});
		});

		// simpan data dari form
 		$("#modal-form-add").on("submit", "#form-product", function() {
			var name = $(this).find('#name').val();
 			var isExist = false;
 			if(isExist==false){
 				$.ajax({
					url : '${contextName}/master/product/save.json',
					type : 'post',
					//this mengacu pada form-product
					data : $(this).serialize(),
					success : function(result) {
						if (result.message == "success") {
							$("#modal-form-add").modal("hide");
							loadData();
							$("#modal-alert-add").modal('show');
						} else if (result.message=="failed"){
							$("#modal-form-add").modal("show");
							var error = '<div class="alert text-danger text-center">Name is already used</div>';
							$("#modal-form-add").find("#checkname").empty().append(error);
						}
					}
				});
 			}
			return false;
		});
		
 		// edit data dari form
 		$("#modal-form-edit").on("submit", "#form-product", function() {
			var name = $(this).find('#name').val();
 			var isExist = false;
 			if(isExist==false){
 				$.ajax({
					url : '${contextName}/master/product/save.json',
					type : 'post',
					//this mengacu pada form-product
					data : $(this).serialize(),
					success : function(result) {
						if (result.message == "success") {
							$("#modal-form-edit").modal("hide");
							loadData();
							$("#modal-alert-edit").modal('show');
						}
					}
				});
 			}
			return false;
		});
		
		// delete data dari form
		$("#modal-delete").on("submit", "#form-product", function() {
			$.ajax({
				url : '${contextName}/master/product/save.json',
				type : 'post',
				//this mengacu pada form-product
				data : $(this).serialize(),
				success : function(result) {
					if (result.message == "success") {
						$("#modal-delete").modal("hide");
						loadData();
						$("#modal-alert-del").modal('show');
					}
				}
			});
			return false;
		});
	});
</script>
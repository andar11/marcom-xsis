<p>
	Delete Data <b>${item.username}</b> ?
</p>
<form id="form-user" method="post" class="form-horizontal">

	<input type="hidden" id="action" name="action" value="delete"/>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" id="id" name="id" value="${item.id}" />
	
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary">Delete</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
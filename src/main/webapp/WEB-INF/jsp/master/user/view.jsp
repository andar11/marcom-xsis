<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-user" method="post" class="form-horizontal">

	<input type="hidden" id="action" name="action" value="update"/>
	<input type="hidden" id="id" name="id" value="${item.id}"/>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="form-group">
				<label class="control-label col-md-4" for="mRoleId">*Role Name</label>
				<div class="col-md-8">
					<input type="text" class="form-control" value="${item.role.name}" readonly="readonly">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4" for="mEmployeeId">*Employee Name</label>
				<div class="col-md-8">
					<input type="text" class="form-control" value="${item.employee.firstName} ${item.employee.lastName}" readonly="readonly">
				</div>
			</div>
		</div>
		
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="form-group">
				<label class="control-label col-md-4" for="username">*Username</label>
				<div class="col-md-8">
					<input type="text" class="form-control" id="username" name="username" value="${item.username}"placeholder="Username" readonly="readonly">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4" for="password">*Password</label>
				<div class="col-md-8">
					<input type="password" class="form-control" id="password" name="password" value="${item.password}"placeholder="Password" readonly="readonly">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4" for="retypepassword">*Re-type Password</label>
				<div class="col-md-8">
					<input type="password" class="form-control" id="retypepassword" name="retypepassword" value="${item.password}" placeholder="Password" readonly="readonly">
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	</div>
</form>
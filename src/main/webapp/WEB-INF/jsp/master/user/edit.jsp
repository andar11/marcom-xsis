<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-user" method="post" class="form-horizontal">

	<!-- validasi saat save -->
	<input type="hidden" id="action" name="action" value="update" /> <input
		type="hidden" id="id" name="id" value="0" /> <input type="hidden"
		name="${_csrf.parameterName}" value="${_csrf.token}" />

	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="mRoleId"><span class="required">*</span>Role Name</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<select id="mRoleId" name="mRoleId" class="form-control">
						<option value="">= --- Select Role Name --- =</option>
						<c:forEach var="item" items="${listRole}">
							<option value="${item.id}">${item.name}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="mEmployeeId"><span class="required">*</span>Employee Name</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<select id="mEmployeeId" name="mEmployeeId" class="form-control">
						<option value="">= --- Select Employee Name --- =</option>
						<c:forEach var="item" items="${listEmployee}">
							<option value="${item.id}">${item.firstName} ${item.lastName}</option>
						</c:forEach>
					</select>
				</div>
			</div>
		</div>
		
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div id="group-name" class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="username"><span class="required">*</span>Username</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" id="username" name="username" required="required"
						class="form-control col-md-7 col-xs-12" value="${item.username}"
						placeholder="Username">
				</div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="password"><span class="required">*</span>Password</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="password" id="password" name="password" required="required" value="${item.password}"
						class="form-control col-md-7 col-xs-12"
						placeholder="Password">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="retypepassword"><span class="required">*</span>Re-type Password</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="password" id="retypepassword" name="retypepassword" required="required" value="${item.password}"
						class="form-control col-md-7 col-xs-12"
						placeholder="Password">
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
<form id="form-product" method="post"
	class="form-horizontal form-label-left input_mask">

	<!-- validasi saat save -->
	<input type="hidden" id="action" name="action" value="insert" /> <input
		type="hidden" id="id" name="id" value="0" /> <input type="hidden"
		name="${_csrf.parameterName}" value="${_csrf.token}" />

			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="code">Product
					Code <span class="required">*</span>
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" name="code" id="code" required="required"
						class="form-control col-md-7 col-xs-12" value="${newCode}" readonly="readonly">
				</div>
			</div>
			
			<div class="form-group" id="group-name">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="name">Product
					Name <span class="required">*</span>
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" name="name" id="name" required="required"
						class="form-control col-md-7 col-xs-12" placeholder="Type Product Name">
				</div>
				<div id="checkname"></div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12"
					for="description">Description </label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<textarea class="form-control" rows="4" name="description" id="description"
						placeholder="Type description"></textarea>
				</div>
			</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
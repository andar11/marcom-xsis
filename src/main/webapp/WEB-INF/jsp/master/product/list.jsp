<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!-- jstl buat format tanggal -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!-- jstl set nilai awal -->
<c:set var="count" value="0" />

<c:forEach var="item" items="${list}">
<!--	jstl setiap kali loop, count +1  -->
<c:set var="count" value="${count + 1}"/>

	<tr>
		<td>${count}</td>
		<td>${item.code}</td>
		<td>${item.name}</td>
		<td>${item.description}</td>
		<td><fmt:formatDate value="${item.createdDate}" pattern="dd/MM/yyyy"/></td>
		<%-- <td>${item.createdDate}</td> --%>
		<td>${item.createdBy}</td>
		<td>
			<button type="button" class="btn btn-primary btn-xs btn-view" value="${item.id}"><i class="fa fa-eye"></i></button>
			<button type="button" class="btn btn-success btn-xs btn-edit" value="${item.id}"><i class="fa fa-edit"></i></button>
			<button type="button" class="btn btn-danger btn-xs btn-delete" value="${item.id}"><i class="fa fa-trash"></i></button>
		</td>
	</tr>
</c:forEach>
	
<form id="form-product" method="post"
	class="form-horizontal form-label-left input_mask">

	<input type="hidden" id="action" name="action" value="update" /> <input
		type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" id="id" name="id" value="${item.id}" />
	
	<div class="modal-header">
		<h4 id="modal-title">View Company - ${item.name} (${item.code})</h4>
	</div>

	<div class="modal-body"></div>
	
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-3 col-xs-12" for="code">Product
			Code <span class="required">*</span>
		</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" name="code" id="code" required="required"
				class="form-control col-md-7 col-xs-12" value="${item.code}" readonly="readonly">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-3 col-xs-12" for="name">Product
			Name <span class="required">*</span>
		</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" name="name" id="name" required="required"
				class="form-control col-md-7 col-xs-12" value="${item.name}" readonly="readonly">
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-4 col-sm-3 col-xs-12"
			for="description">Description </label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea class="form-control" rows="4" name="address" id="address" readonly="readonly"
					>${item.description}</textarea>
		</div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
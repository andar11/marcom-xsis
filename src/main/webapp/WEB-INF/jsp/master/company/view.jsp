<form id="form-company" method="post" class="form-horizontal form-label-left input_mask">

	<input type="hidden" id="action" name="action" value="update" /> 
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" id="id" name="id" value="${item.id}"/>
	
	<div class="modal-header">
		<h4 id="modal-title">View Company - ${item.name} (${item.code})</h4>
	</div>

	<div class="modal-body"></div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="code">Comapny
					Code <span class="required">*</span>
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" name="code" id="code" required="required"
						class="form-control col-md-7 col-xs-12" value="${item.code}" readonly="readonly">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="email">Email
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" id="email" name="email"
						class="form-control col-md-7 col-xs-12" value="${item.email}" readonly="readonly">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="phone">Phone
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" id="phone" name="phone"
						class="form-control col-md-7 col-xs-12" value="${item.phone}" readonly="readonly">
				</div>
			</div>
		</div>
		
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="name">Comapny
					Name <span class="required">*</span>
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" name="name" id="name" required="required"
						class="form-control col-md-7 col-xs-12" value="${item.name}" readonly="readonly">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="address">Address
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<textarea class="form-control" rows="4" name="address" id="address" readonly="readonly"
					>${item.address}</textarea>
				</div>
				
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
	</div>
</form>
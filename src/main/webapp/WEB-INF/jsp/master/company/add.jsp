<form id="form-company" method="post"
	class="form-horizontal form-label-left input_mask" onsubmit="">

	<!-- validasi saat save -->
	<input type="hidden" id="action" name="action" value="insert" /> <input
		type="hidden" id="id" name="id" value="0" /> <input type="hidden"
		name="${_csrf.parameterName}" value="${_csrf.token}" />

	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="code">Comapny
					Code <span class="required">*</span>
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" name="code" id="code" required="required"
						class="form-control col-md-7 col-xs-12" value="${newCode}" readonly="readonly">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="email">Email
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="email" id="email" name="email" onchange="" onclick="" onkeyup="" 
						class="form-control col-md-7 col-xs-12" placeholder="Type Email">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="phone">Phone
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="tel" id="phone" name="phone" pattern="^\+?([0-9]|[-|' '|()])+$"
						class="form-control col-md-7 col-xs-12" placeholder="Type Phone">
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div id="group-name" class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="name">Comapny
					Name <span class="required">*</span>
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" id="name" name="name" required="required"
						class="form-control col-md-7 col-xs-12"
						placeholder="Type Comapny Name">
				</div>
				<div id="checkname"></div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12"
					for="address">Address </label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<textarea class="form-control" rows="4" name="address" id="address"
						placeholder="Type Addres" ></textarea>
				</div>
			</div>
			
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-primary" id="btn-submit">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-employee" method="post" class="form-horizontal form-label-left input_mask">

	<input type="hidden" id="action" name="action" value="update" /> 
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" id="id" name="id" value="${item.id}"/>
	
	<div class="modal-header">
				<h4 id="modal-title">View Company - ${item.firstName} ${item.lastName} (${item.code})</h4>
			</div>
			
	<div class="modal-body"></div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="code">Employee
					ID Number <span class="required">*</span>
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" name="code" id="code" required="required"
						class="form-control col-md-7 col-xs-12" value="${item.code}" readonly="readonly">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="firstName">First
					Name <span class="required">*</span>
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" id="firstName" name="firstName" required="required"
						class="form-control col-md-7 col-xs-12" value="${item.firstName}" readonly="readonly">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="lastName">Last Name
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" id="lastName" name="lastName"
						class="form-control col-md-7 col-xs-12" value="${item.lastName}" readonly="readonly">
				</div>
			</div>
		</div>
		
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="mCompanyId">Company
					Name <span class="required">*</span>
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" id="mCompanyId" name="mCompanyId"
						class="form-control col-md-7 col-xs-12" value="${item.company.name}" readonly="readonly">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="email">Email
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" id="email" name="email"
						class="form-control col-md-7 col-xs-12" value="${item.email}" readonly="readonly">
				</div>
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
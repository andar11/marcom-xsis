<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-employee" method="post"
	class="form-horizontal form-label-left input_mask">

	<!-- validasi saat save -->
	<input type="hidden" id="action" name="action" value="insert" /> <input
		type="hidden" id="id" name="id" value="0" /> <input type="hidden"
		name="${_csrf.parameterName}" value="${_csrf.token}" />

	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="form-group" id="group-code">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="code">Employee
					ID Number <span class="required">*</span>
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" name="code" id="code" required="required"
						class="form-control col-md-7 col-xs-12">
				</div>
				<div id="checkcode"></div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="firstName">First
					Name <span class="required">*</span>
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" name="firstName" id="firstName" required="required"
						class="form-control col-md-7 col-xs-12" placeholder="Type First Name">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="lastName">Last Name
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" id="lastName" name="lastName"
						class="form-control col-md-7 col-xs-12" placeholder="Type Last Name">
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="mCompanyId">Comapny
					Name <span class="required">*</span>
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<select id="mCompanyId" name="mCompanyId" class="form-control">
						<option value="">--Select Company Name--</option>
						<c:forEach var="item" items="${companylist}">
						<option value="${item.id}"> ${item.name}</option>
					</c:forEach>
				</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-4 col-sm-3 col-xs-12" for="email">Email
				</label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="email" id="email" name="email"
						class="form-control col-md-7 col-xs-12" placeholder="Type Email">
				</div>
			</div>
			
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
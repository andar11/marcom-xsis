<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<%
	request.setAttribute("contextName", request.getContextPath());
%>
<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>List User</h2>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<table class="table table-striped">
					<thead>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td align="left">
								<button type="button" id="btn-add" class="btn btn-primary">Add</button>
							</td>
						</tr>
						<tr>	
							<td></td>
							<td><select id="empName" name="empName" class="form-control">
									<option value="">- Select Employee Name -</option>
									<c:forEach var="item" items="${listemp}">
										<option value="${item.employee.id}">${item.employee.fisrtName} ${item.employee.lastName}</option>
									</c:forEach>
							</select></td>

							<td><select id="roleName" name="roleName" class="form-control">
									<option value="">- Select Role Name -</option>
									<c:forEach var="item" items="${listrole}">
										<option value="${item.role.id}">${item.name}</option>
									</c:forEach>
							</select></td>

							<td><select id="compName" name="compName" class="form-control">
									<option value="">- Select Company Name -</option>
									<c:forEach var="item" items="${listemp}">
										<option value="${item.employee.company.id}">${item.name}</option>
									</c:forEach>
							</select></td>

							<td><input type="text" id="txt-search2"
								class="form-control pull-right" placeholder="Username"></td>

							<td>
								<div class="form-group">
									<div class='input-group date'>
										<input type="date" class="form-control" id='datePicker' placeholder="Created"/>
										<span
											class="input-group-addon"> 
											<span
												class="glyphicon glyphicon-calendar">
											</span>
										</span>
									</div>
								</div>
							</td>
							<td><input type="text" id="txt-search3"
								class="form-control pull-right" placeholder="Created By"></td>
								
							<td align="right"><button type="button" id="btn-search"
									class="btn btn-warning pull-right btn-md">Search</button></td>
						</tr>
						<tr>
							<td>No</td>
							<td>Employee</td>
							<td>User</td>
							<td>Company</td>
							<td>Username</td>
							<td>Created Date</td>
							<td>Created By</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody id="list-data">
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="modal-form-add" class="modal fade bs-example-modal-lg"
	tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 id="modal-title">Form User</h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>

<div id="modal-form-edit" class="modal fade bs-example-modal-lg"
	tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 id="modal-title">Form User</h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>

<div id="modal-delete" class="modal fade bs-example-modal-sm"
	tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 id="modal-title">Delete User</h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>

<div id="modal-alert-add" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
			</div> 
			<div class="modal-body">
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                    <p><strong>Data Saved!</strong> New User has been add with code <strong>"${item.code}"</strong></p>
                 </div>
			</div>
		</div>
	</div>
</div>

<div id="modal-alert-edit" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
			</div> 
			<div class="modal-body">
				<div class="alert alert-info alert-dismissible fade in" role="alert">
                    <p><strong>Data Updated!</strong> Data User has been updated !</p>
                 </div>
			</div>
		</div>
	</div>
</div>

<div id="modal-alert-del" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
			</div> 
			<div class="modal-body">
				<div class="alert alert-danger alert-dismissible fade in" role="alert">	
                    <strong>Data Deleted!</strong> Data User with code <strong>"${item.code}"</strong> has been deleted !
                 </div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var token = '${_csrf.token}';
	/*
	 * Ajax Setup ini dilakukan agar setiap kali ada 
	 * POST ajax call akan menyertakan header X-CSRF-TOKEN.
	 * Ini perlu dilakukan apabila fitur csrf diaktifkan. Untuk mengetahui
	 * tentang apa itu csrf silakan googling sendiri
	 * 
	 * fitur csrf diaktifkan ada di applicationContext-security.xml line 38
	 */ 
	 function ajaxSetup(token) {
		$.ajaxSetup({
			headers : {
				'X-CSRF-TOKEN' : token
			}
		});
	}
	
	$(document).ready(function() {
		ajaxSetup(token);
	})

	// menampilkan data untuk pertama kali	
	function loadData() {
		$.ajax({
			url : '${contextName}/master/user/list',
			dataType : 'html',
			type : 'get',
			success : function(result) {
				$("#list-data").html(result);
			}
		});
	}

	$(document).ready(function() {
		// load data first display
		loadData();

		$("#btn-add").click(function() {
			$.ajax({
				url : '${contextName}/master/user/add',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$("#modal-form-add").find(".modal-body").html(result);
					$("#modal-title").html("Add New User");
					$("#modal-form-add").removeClass("modal-danger");
					$("#modal-form-add").modal("show");
				}
			});
		});

		//pencarian
		$("#btn-search").click(function() {
			var key = $("#txt-search").val();
			var key1 = $("#txt-search1").val();
			var datePicker = $("#datePicker").val();
			var key2 = $("#txt-search2").val();
			$.ajax({
				url : '${contextName}/master/user/search',
				data : {
					'key' : key,
					'key1' : key1,
					'datePicker' : datePicker,
					'key2' : key2
				},
				dataType : 'html',
				type : 'get',
				success : function(result) {
					$("#list-data").html(result);
				}
			});
		});

		//view
		$("#list-data").on('click', '.btn-view', function() {
			var id = $(this).val();
			$.ajax({
				url : '${contextName}/master/user/view',
				data : {
					'id' : id
				},
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$("#modal-form-edit").find(".modal-body").html(result);
					$("#modal-title").html("View User");
					$("#modal-form-edit").removeClass("modal-danger");
					$("#modal-form-edit").modal("show");
				}
			});
		});

		$("#list-data").on('click', '.btn-edit', function() {
			var id = $(this).val();
			$.ajax({
				url : '${contextName}/master/user/edit',
				data : {
					'id' : id
				},
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$("#modal-form-edit").find(".modal-body").html(result);
					$("#modal-title").html("Edit User");
					$("#modal-form-edit").removeClass("modal-danger");
					$("#modal-form-edit").modal("show");
					//$("#modal-alert-edit").find(".modal-body");
					//$("#modal-title-alert").html("Update");
				}
			});
		});

		$("#list-data").on('click', '.btn-delete', function() {
			var id = $(this).val();
			$.ajax({
				url : '${contextName}/master/user/delete',
				data : {
					'id' : id
				},
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$("#modal-delete").find(".modal-body").html(result);
					$("#modal-title").html("Delete User");
					$("#modal-delete").addClass("modal-danger");
					$("#modal-delete").modal("show");
				}
			});
		});

		// simpan data dari form
		$("#modal-form-add").on("submit", "#form-user", function() {
			$.ajax({
				url : '${contextName}/master/user/save.json',
				type : 'post',
				data : $(this).serialize(),
				success : function(result) {
					if (result.message == "success") {
						$("#modal-form-add").modal("hide");
						loadData();
						$("#modal-alert-add").modal('show');
					}
				}
			});
			return false;
		});

		//edit data dari form
		$("#modal-form-edit").on("submit", "#form-user", function() {
			$.ajax({
				url : '${contextName}/master/user/save.json',
				type : 'post',
				data : $(this).serialize(),
				success : function(result) {
					if (result.message == "success") {
						$("#modal-form-edit").modal("hide");
						loadData();
						$("#modal-alert-edit").modal('show');
					}
				}
			});
			return false;
		});

		//delete data dari form
		$("#modal-delete").on("submit", "#form-user", function() {
			$.ajax({
				url : '${contextName}/master/user/save.json',
				type : 'post',
				data : $(this).serialize(),
				success : function(result) {
					if (result.message == "success") {
						$("#modal-delete").modal("hide");
						loadData();
						$("#modal-alert-del").modal('show');
					}
				}
			});
			return false;
		});
	});
</script>
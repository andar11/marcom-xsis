<p>
	Delete Data <b>${item.name}</b> ?
</p>
<form id="form-role" method="post" class="form-horizontal form-label-left input_mask">

	<input type="hidden" id="action" name="action" value="delete"/>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" id="id" name="id" value="${item.id}" />
	
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary">Delete</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
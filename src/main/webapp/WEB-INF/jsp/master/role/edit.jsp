<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-role" method="post" class="form-horizontal form-label-left input_mask">

	<input type="hidden" id="action" name="action" value="update"/>
	<input type="hidden" id="id" name="id" value="${item.id}"/>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	
	<div class="form-group">
		<div class="col-md-12">
			<div class="col-sm-3">
				<label class="control-label"><span class="required">*</span>ROLE CODE</label>
			</div>
			<div class="col-sm-8">
				<input class="form-control" id="code"  value="${item.code}" name="code" readonly="readonly">
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-12">
			<div class="col-sm-3">
				<label class="control-label"><span class="required">*</span>ROLE NAME</label>
			</div>
			<div class="col-sm-8">
				<input class="form-control" id="name" value="${item.name}" name="name">
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-md-12">
			<div class="col-sm-3">
				<label class="control-label">DESCRIPTION</label>
			</div>
			<div class="col-sm-8">
				<input class="form-control" id="description"  value="${item.description}" name="description">
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
		<button type="submit" class="btn btn-primary"><i data-dismiss="modal"></i>Update</button>
	</div>
</form>
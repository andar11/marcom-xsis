<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-role" method="post" class="form-horizontal">

	<input type="hidden" id="action" name="action" value="update"/>
	<input type="hidden" id="id" name="id" value="${item.id}"/>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	
	<!-- <div class="row"> -->
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"> 
			<div class="form-group" col-md-4>
				<label class="control-label col-md-4" for="code">*Role Code</label>
				<div class="col-md-8">
					<input type="text" class="form-control" name="code" value="${item.code}" readonly="readonly">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4" for="name">*Role Name</label>
				<div class="col-md-8">
					<input type="text" class="form-control" name="name" value="${item.name}" readonly="readonly">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4" for="description">Description</label>
				<div class="col-md-8">
					<input type="text" class="form-control" id="description" name="description" value="${item.description}" readonly="readonly">
				</div>
			</div>
		</div>
	<!-- </div> -->
	
	<div class="modal-footer">
		<button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
	</div>
</form>
<form id="form-role" method="post" class="form-horizontal form-label-left input_mask">

	<!-- validasi saat save -->
	<input type="hidden" id="action" name="action" value="insert" /> <input
		type="hidden" id="id" name="id" value="0" /> <input type="hidden"
		name="${_csrf.parameterName}" value="${_csrf.token}" />

	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="form-group">
				<div class="col-md-4 col-sm-3 col-xs-12">
					<label class="control-label"><span class="required">*</span>ROLE
						CODE</label>
				</div>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" class="form-control" id="code" name="code"
						value="${newCode}" readonly="readonly">
				</div>
			</div>
			
			<div id="group-name" class="form-group">
				<div class="col-md-4 col-sm-3 col-xs-12">
					<label class="control-label"><span class="required">*</span>ROLE
						NAME</label>
				</div>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="text" class="form-control" id="name" name="name" placeholder="Type Role Name">
				</div>
			</div>
			
			<div id="group-name" class="form-group">
				<div class="col-md-4 col-sm-3 col-xs-12">
					<label class="control-label">DESCRIPTION</label>
				</div>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<input type="tel" class="form-control" id="description" name="description" placeholder="Type Description">
				</div>
			</div>
		</div>	
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
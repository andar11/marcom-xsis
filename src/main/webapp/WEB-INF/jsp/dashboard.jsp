<%
	request.setAttribute("contextName", request.getContextPath());
%>
<div class="dashboard_graph">

	<div class="row x_title">
		<div class="col-md-6">
			<h3>
				Network Activities <small>Graph title sub-title</small>
			</h3>
		</div>
		<div class="col-md-6">
			<div id="reportrange" class="pull-right"
				style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
				<i class="glyphicon glyphicon-calendar fa fa-calendar"></i> <span>December
					30, 2014 - January 28, 2015</span> <b class="caret"></b>
			</div>
		</div>
	</div>

	<div class="col-md-9 col-sm-9 col-xs-12">
		<div id="chart_plot_01" class="demo-placeholder"></div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 bg-white">
		<div class="x_title">
			<h2>Top Campaign Performance</h2>
			<div class="clearfix"></div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-6">
			<div>
				<p>Facebook Campaign</p>
				<div class="">
					<div class="progress progress_sm" style="width: 76%;">
						<div class="progress-bar bg-green" role="progressbar"
							data-transitiongoal="80"></div>
					</div>
				</div>
			</div>
			<div>
				<p>Twitter Campaign</p>
				<div class="">
					<div class="progress progress_sm" style="width: 76%;">
						<div class="progress-bar bg-green" role="progressbar"
							data-transitiongoal="60"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-6">
			<div>
				<p>Conventional Media</p>
				<div class="">
					<div class="progress progress_sm" style="width: 76%;">
						<div class="progress-bar bg-green" role="progressbar"
							data-transitiongoal="40"></div>
					</div>
				</div>
			</div>
			<div>
				<p>Bill boards</p>
				<div class="">
					<div class="progress progress_sm" style="width: 76%;">
						<div class="progress-bar bg-green" role="progressbar"
							data-transitiongoal="50"></div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="clearfix"></div>
</div>

<button type="button" class="btn btn-primary" id="btn-show-modal">Large modal</button>

<div id="modal-test" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
	aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Modal title</h4>
			</div>
			<div class="modal-body">
				<h4>Text in a modal</h4>
				<p>Praesent commodo cursus magna, vel scelerisque nisl
					consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum
					faucibus dolor auctor.</p>
				<p>Aenean lacinia bibendum nulla sed consectetur. Praesent
					commodo cursus magna, vel scelerisque nisl consectetur et. Donec
					sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$('#btn-show-modal').click(function(){
		$('#modal-test').modal('show');
	});
</script>
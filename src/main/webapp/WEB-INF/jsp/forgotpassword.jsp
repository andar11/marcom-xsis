<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%
	request.setAttribute("contextName", request.getContextPath());
%>
<p class="login-box-msg">Start your session for reset password</p>

<form id="form-forgotpassword" name="forgotForm" role="form">
	<input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
	<h1>Forgot Password</h1>
	<div>
		<input type="text" name="username" class="form-control" placeholder="Username"/>
	</div>
	<div>
		<input type="password" name="password" class="form-control"
			id="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$"
			placeholder="New Password" required />
	</div>
	<div>
		<input type="password" name="repassword" class="form-control"
			id="repassword" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$"
			placeholder="Confirm Password" required />
	</div>
	<div>
		<button type="submit" class="btn btn-primary submit">Reset</button>
		<a class="loginForm" href="${contextName}/login.html" >Log in</a>
	</div>

	<div class="clearfix"></div>

	<div class="separator">
		<!-- <p class="change_link">
			New to site? <a href="#signup" class="to_register"> Create
				Account </a>
		</p> -->

		<div class="clearfix"></div>
		<br />

		<div>
			<h1>
				<i class="fa fa-paw"></i> MARCOM!
			</h1>
			<p>�2016 All Rights Reserved.</p>
		</div>
	</div>
</form>

<script>
	$(document).ready(function() {
		$('#form-forgotpassword').on('submit', function() {
			$.ajax({
				url : '${contextName}/savepassword.json',
				type : 'post',
				data : $(this).serialize(),
				success : function(result) {
					if (result.message == "success") {
						alert("Password has been changed. Please click Login");
						$('#form-forgotpassword').get(0).reset()
					}
					else if (result.message == "notsame") {
						alert("Passwords do not match");
					}
					else if(result.message == "username_invalid"){
						alert("Username not available");
						$('#form-forgotpassword').get(0).reset()
					}
				}
			});
			return false;
		});
	});
</script>
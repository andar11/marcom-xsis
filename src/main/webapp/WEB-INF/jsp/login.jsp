<%
	request.setAttribute("contextName", request.getContextPath());
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<p class="login-box-msg">Sign in to start your session</p>
<c:if test="${not empty error}">
	<div class="error">${error}</div>
</c:if>
<c:if test="${not empty msg}">
	<div class="msg">${msg}</div>
</c:if>
<form name='loginForm' action="<c:url value='/j_spring_security_check' />" method='POST'>
	<input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
	<h1>Login Form</h1>
	<div>
		<input type="text" name="username" class="form-control" placeholder="Username"
			required="" />
	</div>
	<div>
		<input type="password" name="password" class="form-control" placeholder="Password"
			required="" />
	</div>
	<div>
		<button type="submit" class="btn btn-success submit">Log in</button> 
		<a class="reset_pass" href="${contextName}/forgotpassword.html">Forgot your password?</a>
	</div>

	<div class="clearfix"></div>

	<div class="separator">
		<!-- <p class="change_link">
			New to site? <a href="#signup" class="to_register"> Create
				Account </a>
		</p> -->

		<div class="clearfix"></div>
		<br />

		<div>
			<h1>
				<i class="fa fa-paw"></i> MARCOM!
			</h1>
			<p>�2016 All Rights Reserved.</p>
		</div>
	</div>
</form>
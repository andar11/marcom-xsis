<%
	request.setAttribute("contextName", request.getContextPath());
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<sec:authorize access="isAuthenticated()">
	<sec:authentication property="principal.username" var="username" />
</sec:authorize>
<sec:authorize access="!isAuthenticated()">
	<sec:authentication property="principal" var="username" />
</sec:authorize>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Gentelella Alela! |</title>

<!-- Bootstrap -->
<link href="${contextName}/assets/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Font Awesome -->
<link href="${contextName}/assets/css/font-awesome.min.css"
	rel="stylesheet">
<!-- NProgress -->
<link href="${contextName}/assets/css/nprogress.css" rel="stylesheet">
<!-- Animate.css -->
<link href="${contextName}/assets/css/animate.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="${contextName}/assets/css/custom.min.css" rel="stylesheet">

<!-- jQuery -->
<script src="${contextName}/assets/js/jquery.min.js"></script>

</head>

<body class="login">
	<div>
		<a class="hiddenanchor" id="signup"></a> <a class="hiddenanchor"
			id="signin"></a>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<decorator:body></decorator:body>
				</section>
			</div>

		</div>
	</div>
</body>
</html>

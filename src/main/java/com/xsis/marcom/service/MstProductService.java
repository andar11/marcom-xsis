package com.xsis.marcom.service;

import java.util.List;

import com.xsis.marcom.model.MstProductModel;

public interface MstProductService {
	public Boolean checkName(String name) throws Exception;
	public String getCode() throws Exception;
	public List<MstProductModel> get() throws Exception;
	public List<MstProductModel> search(String keySearch, String keyName, String keyDescrip, String createdDate, String keyCreatedBy) throws Exception;
	public MstProductModel getById(int id) throws Exception;
	public void insert(MstProductModel model) throws Exception;
	public void update(MstProductModel model) throws Exception;
	public void delete(MstProductModel model) throws Exception;

}

package com.xsis.marcom.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.marcom.dao.RoleDao;
import com.xsis.marcom.model.RoleModel;

@Service
@Transactional
public class RoleServiceImpl implements RoleService{

	@Autowired
	private RoleDao dao;
	
	@Override
	public String getCode() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getCode();
	}
	

	@Override
	public List<RoleModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public List<RoleModel> search(String keySearch, String keyName, String createdDate, String keyCreatedBy) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.search(keySearch, keyName, createdDate, keyCreatedBy);
	}

	@Override
	public RoleModel getById(Integer id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void insert(RoleModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public void update(RoleModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(RoleModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

}

package com.xsis.marcom.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.xsis.marcom.dao.UserDao;
import com.xsis.marcom.model.UserModel;

@Service
@Transactional
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserDao dao;
	
	@Override
	public List<UserModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public List<UserModel> search(String keyEmpname, String keyRolname, String keyCompname, String keyUsername, String createdDate, String keyCreatedBy) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.search(keyEmpname, keyRolname, keyCompname, keyUsername, createdDate, keyCreatedBy);
	}

	@Override
	public UserModel getById(Integer id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void insert(UserModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public void update(UserModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(UserModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public UserModel getByUsername(String username) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getByUsername(username);
	}

	
}

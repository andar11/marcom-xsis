package com.xsis.marcom.service;

import java.util.List;
import com.xsis.marcom.model.UserModel;

public interface UserService {

	public List<UserModel> get() throws Exception;
	public List<UserModel> search(String keyEmpname, String keyRolname, String keyCompname, String keyUsername, String createdDate, String keyCreatedBy) throws Exception;
	public UserModel getById(Integer id) throws Exception;
	public UserModel getByUsername(String username) throws Exception;
	public void insert(UserModel model) throws Exception;
	public void update(UserModel model) throws Exception;
	public void delete(UserModel model) throws Exception;
}

package com.xsis.marcom.common;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StringConverter {

	public static String inToString(int num, int digits) {
		String output = Integer.toString(num);
		while (output.length() < digits) output = "0" + output;
		return output;
	}
	
	public static String datecode(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy");
		return sdf.format(date);
	}
}

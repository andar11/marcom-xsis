package com.xsis.marcom.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="M_ROLE")
public class RoleModel {

	private Integer id;
	private String code;
	private String name;
	private String description;
	private Integer isDelete;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	private List<UserModel> listUser;
	
	@Id
	@Column(name="ID", nullable = true)
	@GeneratedValue(strategy=GenerationType.TABLE,generator="M_ROLE")
	@TableGenerator(name="M_ROLE",table="MST_SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="M_ROLE", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=0)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="CODE", nullable = true)
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code; 
	}
	
	@Column(name="NAME", nullable = true)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="DESCRIPTION", nullable = true)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="IS_DELETE", nullable = true)
	public Integer getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
	
	@Column(name="CREATED_BY", nullable = true)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE", nullable = true)
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="UPDATE_BY", nullable = true)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="UPDATE_DATE", nullable = true)
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@OneToMany(targetEntity=UserModel.class, fetch=FetchType.EAGER, mappedBy="role")
	public List<UserModel> getListUser() {
		return listUser;
	}
	public void setListUser(List<UserModel> listUser) {
		this.listUser = listUser;
	}
	
	
}

package com.xsis.marcom.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "M_USER")
public class UserModel {

	private Integer id;
	private String username;
	private String password;
	private String mRoleId;
	private String mEmployeeId;
	private Integer isDelete;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	private RoleModel role;
	private MstEmployeeModel employee;

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "M_USER")
	@TableGenerator(name = "M_USER", table = "MST_SEQUENCE", pkColumnName = "SEQUENCE_ID", pkColumnValue = "M_USER", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 0)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "USERNAME", nullable = true)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "PASSWORD", nullable = true)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "M_ROLE_ID", nullable = true)
	public String getmRoleId() {
		return mRoleId;
	}

	public void setmRoleId(String mRoleId) {
		this.mRoleId = mRoleId;
	}
	
	@Column(name = "M_EMPLOYEE_ID", nullable = true)
	public String getmEmployeeId() {
		return mEmployeeId;
	}

	public void setmEmployeeId(String mEmployeeId) {
		this.mEmployeeId = mEmployeeId;
	}
	
	@Column(name = "IS_DELETE", nullable = true)
	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	
	@Column(name = "CREATED_BY", nullable = true)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE", nullable = true)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name = "UPDATE_BY", nullable = true)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "UPDATE_DATE", nullable = true)
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="M_ROLE_ID", nullable=false, insertable=false, updatable=false)
	public RoleModel getRole() {
		return role;
	}

	public void setRole(RoleModel role) {
		this.role = role;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="M_EMPLOYEE_ID", nullable=false, insertable=false, updatable=false)
	public MstEmployeeModel getEmployee() {
		return employee;
	}

	public void setEmployee(MstEmployeeModel employee) {
		this.employee = employee;
	}

}

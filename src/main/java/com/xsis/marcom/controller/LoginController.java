package com.xsis.marcom.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.marcom.model.UserModel;
import com.xsis.marcom.service.UserService;

@Controller
public class LoginController {

	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private UserService service;
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value="error", required=false) String error,
			@RequestParam(value="logout",required=false) String logout){
		ModelAndView model = new ModelAndView();
		if(error != null){
			model.addObject("error","Invalid username and password");
		}
		if(logout != null){
			model.addObject("msg","You are have logged out successfully");
		}
		return model;
	}
	
	
	@RequestMapping(value="/403", method=RequestMethod.GET)
	public ModelAndView AccessDenied(){
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(!(auth instanceof AnonymousAuthenticationToken)){
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addObject("username", userDetail.getUsername());
		}
		return model;
	}
	
	@RequestMapping(value="/forgotpassword",  method=RequestMethod.GET)
	public ModelAndView forgetPassword() {
		return new ModelAndView("forgotpassword");
	}
	
	@RequestMapping(value="/savepassword",  method=RequestMethod.POST)
	public String savePassword(Model model, HttpServletRequest request) {
		String result = "";
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");
		UserModel user = null;
		try {
			user = this.service.getByUsername(username);
			if (user != null) {
				if(password.equals(repassword)) {
					user.setPassword(password);
					this.service.update(user);
					result = "success";
					
				} else {
					result = "notsame";
				}
			}
			else {
				result = "username_invalid";
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			result = "failed";
		}
		model.addAttribute("message", result);
		
		return "savepassword";
	}
}
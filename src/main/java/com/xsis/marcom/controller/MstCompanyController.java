package com.xsis.marcom.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.marcom.model.MstCompanyModel;
import com.xsis.marcom.service.MstCompanyService;

@Controller
public class MstCompanyController extends BaseController {
	
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private MstCompanyService service;
	
	@RequestMapping(value="/master/company")
	public ModelAndView CompanyIndex(Model model) {
		Collection<MstCompanyModel> result = null;
		try {
			result = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", result);
		return new ModelAndView("/master/company");
	}
	
	@RequestMapping(value = "/master/company/list", method = RequestMethod.GET)
	public ModelAndView list(Model model, HttpServletRequest request) {
		Collection<MstCompanyModel> result = null;
		try {
			result = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", result);

		return new ModelAndView("/master/company/list");
	}
	
	@RequestMapping(value = "/master/company/checkName", method = RequestMethod.GET)
	public ModelAndView checkName(Model model, HttpServletRequest request) {
		String vName = request.getParameter("name");
		Boolean result = false;
		try {
			result = this.service.checkName(vName);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("message", result);
		
		return new ModelAndView("/master/company/checkName");
	}

	@RequestMapping(value = "/master/company/search", method = RequestMethod.GET)
	public ModelAndView search(Model model, HttpServletRequest request) throws ParseException {
		String keySearch = request.getParameter("key");
		String keyName = request.getParameter("key1");
		String createdDate = request.getParameter("datePicker");
		String keyCreatedBy = request.getParameter("key2");
		Collection<MstCompanyModel> result = null;
		try {
			result = this.service.search(keySearch, keyName, createdDate, keyCreatedBy);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", result);

		return new ModelAndView("/master/company/list");
	}

	@RequestMapping(value = "/master/company/add", method = RequestMethod.GET)
	public ModelAndView add(Model model) {
		String code ="CP0001";
		try {
			code = this.service.getCode();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("newCode", code);
		
		return new ModelAndView("/master/company/add");
	}

	@RequestMapping(value = "/master/company/edit")
	public ModelAndView edit(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		MstCompanyModel result = new MstCompanyModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);
		
		return new ModelAndView("/master/company/edit");
	}
	
	@RequestMapping(value = "/master/company/view")
	public ModelAndView view(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		MstCompanyModel result = new MstCompanyModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);
		
		return new ModelAndView("/master/company/view");
	}

	@RequestMapping(value = "/master/company/delete")
	public ModelAndView delete(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		MstCompanyModel result = new MstCompanyModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/company/delete");
	}

	@RequestMapping(value = "/master/company/save")
	public String save(Model model, @ModelAttribute MstCompanyModel item, HttpServletRequest request) {
		String result = "";
		String action = request.getParameter("action");
		MstCompanyModel tmp = null;
		try {
			
			tmp = this.service.getById(item.getId());

			if (action.equals("insert")){
				item.setCreatedDate(new Date());
				item.setUpdatedDate(new Date());
				item.setCreatedBy(new String(this.getUserName()));
				item.setUpdatedBy(new String(this.getUserName()));
				item.setIsDelete(0);
				if(!this.service.checkName(item.getName())) {
					this.service.insert(item);
					result = "success";
				} else {
					result = "failed";
				}
				
			} else if (action.equals("update")){
				item.setCreatedDate(new Date());
				item.setUpdatedDate(new Date());
				item.setCreatedBy(new String(this.getUserName()));
				item.setUpdatedBy(new String(this.getUserName()));
				item.setIsDelete(new Integer(tmp.getIsDelete()));
				this.service.update(item);
				result = "success";
				
			} else if (action.equals("delete")) {
				tmp.setIsDelete(1);
				this.service.update(tmp);
				result = "success";
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "failed";
		}
		model.addAttribute("message", result);

		return "/master/company/save";
	}
}

package com.xsis.marcom.controller;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.marcom.model.RoleModel;
import com.xsis.marcom.service.RoleService;

@Controller
public class RoleController extends BaseController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private RoleService service;

	@RequestMapping(value = "/master/role")
	public ModelAndView RoleIndex(Model model) {
		Collection<RoleModel> result = null;
		try {
			result = this.service.get();
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", result);
		return new ModelAndView("/master/role");
	}

	@RequestMapping(value = "/master/role/list", method = RequestMethod.GET)
	public ModelAndView list(Model model) {
		Collection<RoleModel> result = null;
		try {
			result = this.service.get();
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", result);

		return new ModelAndView("/master/role/list");
	}

	@RequestMapping(value = "/master/role/search", method = RequestMethod.GET)
	public ModelAndView search(Model model, HttpServletRequest request) throws ParseException {
		String keySearch = request.getParameter("key");
		String keyName=request.getParameter("key1");
		String createdDate=request.getParameter("datePicker");
		String keyCreatedBy=request.getParameter("key2");
		Collection<RoleModel> result = null;
		RoleModel r = new RoleModel();
		try {
			if(keyCreatedBy == "" && (keySearch!="" || keyName!="" || createdDate!="")) {
				keyCreatedBy = r.getCreatedBy();
				result = this.service.search(keySearch, keyName, createdDate, keyCreatedBy);
			}
			else if (keyCreatedBy == "" && keySearch=="" && keyName=="" && createdDate=="") {
				result=this.service.get();
			}
			else {
				result = this.service.search(keySearch, keyName, createdDate, keyCreatedBy);
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", result);

		return new ModelAndView("/master/role/list");

	}

	@RequestMapping(value = "/master/role/add", method = RequestMethod.GET)
	public ModelAndView add(Model model) {
		String code = "RO0001";
		try {
			code = this.service.getCode();
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
		}
		model.addAttribute("newCode", code);
		return new ModelAndView("/master/role/add");
	}

	@RequestMapping(value = "/master/role/edit")
	public ModelAndView edit(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		RoleModel result = new RoleModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/role/edit");
	}
	
	@RequestMapping(value = "/master/role/view")
	public ModelAndView detail(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		RoleModel result = new RoleModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/role/view");
	}
	

	@RequestMapping(value = "/master/role/delete")
	public ModelAndView delete(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		RoleModel result = new RoleModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/role/delete");
	}

	@RequestMapping(value = "/master/role/save")
	public String save(Model model, @ModelAttribute RoleModel item, HttpServletRequest request) {
		String result = "";
		String action = request.getParameter("action");
		RoleModel tmp = null;
		
		try {
			tmp = this.service.getById(item.getId());
			if (action.equals("insert")) {
				item.setCreatedDate(new Date());
				item.setUpdatedDate(new Date());
				item.setCreatedBy(new String(this.getUserName()));
				item.setUpdatedBy(new String(this.getUserName()));
				item.setIsDelete(new Integer(0));
				this.service.insert(item);
			} else if (action.equals("update")) {
				item.setUpdatedDate(new Date());
				item.setUpdatedBy(new String(this.getUserName()));
				item.setCreatedBy(new String(this.getUserName()));
				item.setCreatedDate(tmp.getCreatedDate());
				item.setIsDelete(new Integer(tmp.getIsDelete()));
				this.service.update(item);
			} else if (action.equals("delete")) {
				tmp.setIsDelete(1);
				this.service.update(tmp);
			};

			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "failed";
		}

		model.addAttribute("message", result);

		return "/master/role/save";
	}
}

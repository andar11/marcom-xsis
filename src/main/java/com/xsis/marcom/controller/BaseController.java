/**
 * 
 */
package com.xsis.marcom.controller;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;

import com.xsis.marcom.service.UserService;
import com.xsis.marcom.model.UserModel;

@Controller
public class BaseController {
	
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private UserService service;
	
	public String getUserName() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth!=null) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			return userDetail.getUsername();
		}
		return null;
	}
	
	public int getUserId() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth!=null) {
			User user = (User) auth.getPrincipal();
			return 0;
		}
		return 0;
	}
	
	public UserModel getUserModel(HttpSession session){
		UserModel result = null;
		if(session.getAttribute("user")!=null){
			result = (UserModel)session.getAttribute("user");
		}else {
			try {
				result = service.getById(this.getUserId());
				session.setAttribute("user",result);
			} catch (Exception e) {
				log.error(e.getMessage(),e);
			}
		}
		return result;
	}
}

package com.xsis.marcom.controller;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.marcom.model.MstEmployeeModel;
import com.xsis.marcom.model.RoleModel;
import com.xsis.marcom.model.UserModel;
import com.xsis.marcom.service.MstEmployeeService;
import com.xsis.marcom.service.RoleService;
import com.xsis.marcom.service.UserService;

@Controller
public class UserController extends BaseController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private UserService service;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private MstEmployeeService employeeService;
	
	@RequestMapping(value = "/master/user")
	public ModelAndView userIndex(Model model) {
		Collection<UserModel> result = null;
		try {
			result = this.service.get();
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", result);
		return new ModelAndView("/master/user");
	}

	@RequestMapping(value = "/master/user/list", method = RequestMethod.GET)
	public ModelAndView list(Model model) {
		Collection<UserModel> result = null;
		List<MstEmployeeModel> resultEmp = null;
		List<RoleModel> resultRole = null;
		try {
			result = this.service.get();
			resultEmp = this.employeeService.get();
			resultRole = this.roleService.get();
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", result);
		model.addAttribute("listemp", resultEmp);
		model.addAttribute("listrole", resultRole);

		return new ModelAndView("/master/user/list");
	}

	@RequestMapping(value = "/master/user/search", method = RequestMethod.GET)
	public ModelAndView search(Model model, HttpServletRequest request)throws ParseException {
		String keyEmpname = request.getParameter("emp");
		String keyRolname=request.getParameter("rol");
		String keyCompname=request.getParameter("comp");
		String keyUsername=request.getParameter("user");
		String createdDate=request.getParameter("datePicker");
		String keyCreatedBy=request.getParameter("key2");
		Collection<UserModel> result = null;
		try {
			result = this.service.search(keyEmpname, keyRolname, keyCompname, keyUsername, createdDate, keyCreatedBy);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", result);

		return new ModelAndView("/master/user/list");

	}

	@RequestMapping(value = "/master/user/add", method = RequestMethod.GET)
	public ModelAndView add(Model model) {
		List<RoleModel> roleList = null;
		List<MstEmployeeModel> employeeList = null;
		try {
			roleList = roleService.get();
			employeeList = employeeService.get();
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("listRole", roleList);
		model.addAttribute("listEmployee", employeeList);
		return new ModelAndView("/master/user/add");
	}

	@RequestMapping(value = "/master/user/edit")
	public ModelAndView edit(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		UserModel result = new UserModel();
		List<RoleModel> roleList = null;
		List<MstEmployeeModel> employeeList = null;
		try {
			result = this.service.getById(id);
			roleList = roleService.get();
			employeeList = employeeService.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);
		model.addAttribute("listRole", roleList);
		model.addAttribute("listEmployee", employeeList);

		return new ModelAndView("/master/user/edit");
	}
	
	@RequestMapping(value = "/master/user/view")
	public ModelAndView view(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		UserModel result = new UserModel();
		List<RoleModel> roleList = null;
		List<MstEmployeeModel> employeeList = null;
		try {
			result = this.service.getById(id);
			roleList = roleService.get();
			employeeList = employeeService.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);
		model.addAttribute("listRole", roleList);
		model.addAttribute("listEmployee", employeeList);

		return new ModelAndView("/master/user/view");
	}

	@RequestMapping(value = "/master/user/delete")
	public ModelAndView delete(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		UserModel result = new UserModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/user/delete");
	}

	@RequestMapping(value = "/master/user/save")
	public String save(Model model, @ModelAttribute UserModel item, HttpServletRequest request) {
		String result = "";
		String action = request.getParameter("action");
		UserModel tmp = null;
		
		try {
			tmp = this.service.getById(item.getId());
			if (action.equals("insert")) {
				item.setCreatedDate(new Date());
				item.setCreatedBy(new String(this.getUserName()));
				item.setUpdatedDate(new Date());
				item.setUpdatedBy(new String(this.getUserName()));
				item.setIsDelete(new Integer(0));
				this.service.insert(item);
			} else if (action.equals("update")) {
				item.setUpdatedDate(new Date());
				item.setUpdatedBy(new String(this.getUserName()));
				item.setCreatedBy(new String(this.getUserName()));
				item.setCreatedDate(tmp.getCreatedDate());
				item.setIsDelete(new Integer(tmp.getIsDelete()));
				this.service.update(item);
			} else if (action.equals("delete")) {
				tmp.setIsDelete(1);
				this.service.update(tmp);
			};

			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "failed";
		}

		model.addAttribute("message", result);

		return "/master/user/save";
	}
}

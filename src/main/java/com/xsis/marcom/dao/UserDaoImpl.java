package com.xsis.marcom.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.marcom.model.UserModel;

@Repository
public class UserDaoImpl implements UserDao{

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<UserModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<UserModel> result = session.createQuery("from UserModel where isDelete=0").list();
		return result;
	}

	@Override
	public List<UserModel> search(String keyEmpname, String keyRolname, String keyCompname, String keyUsername, String createdDate, String keyCreatedBy) throws Exception {
		Session session1 = sessionFactory.getCurrentSession();
		List<UserModel> result1 = session1.createQuery("from UserModel where (employeeid='"+keyEmpname+"'"
				+ " or roleid='"+keyRolname+"'"
				+ " or employeeid='"+keyCompname+"'"
				+ " or username='"+keyUsername+"'"
				+ " or TRUNC(createdDate) = TO_DATE('"+createdDate+"', 'yyyy-MM-dd')"
				+ " or createdBy = '"+keyCreatedBy+"') and isDelete = 0").list();
		return result1;
	}

	@Override
	public UserModel getById(Integer id) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		UserModel result = session.get(UserModel.class, id);
		return result;
	}

	@Override
	public void insert(UserModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(model);
	}

	@Override
	public void update(UserModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(model);
	}

	@Override
	public void delete(UserModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(model);
	}

	@Override
	public UserModel getByUsername(String username) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		/*Criteria criteria = session.createCriteria(UserModel.class);
		criteria.add(Restrictions.eq("username", username));*/
		Query query = session.createQuery("from UserModel where username = :username");
		query.setParameter("username", username);
		UserModel result = (UserModel)query.uniqueResult();
		return result;
	}
}

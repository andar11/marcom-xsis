package com.xsis.marcom.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.marcom.common.StringConverter;
import com.xsis.marcom.model.RoleModel;

@Repository
public class RoleDaoImpl implements RoleDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public String getCode() throws Exception {
		// TODO Auto-generated method stub
		String result = "";
		Session session = sessionFactory.getCurrentSession();
		List list = session.createQuery("select max(r.code) from RoleModel r").list();
		result = list.get(0).toString();
		if (result.equals("")) {
			result = "RO001";
		} else {
			String[] arrCode = result.split("O");
			int newCode = Integer.parseInt(arrCode[1]) + 1;
			result = "RO" + StringConverter.inToString(newCode, 3);
		}
		return result;
	}

	@Override
	public List<RoleModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<RoleModel> result = session.createQuery("from RoleModel where isDelete=0").list();
		return result;
	}
	@Override
	public List<RoleModel> search(String keySearch,String keyName,String createdDate,String keyCreatedBy) throws Exception {
//		Session session=sessionFactory.getCurrentSession();
//		Criteria criteria=session.createCriteria(MenuModel.class);
//		criteria.add(Restrictions.like("code", "%"+ keySearch +"%"));
//		List<MenuModel> result=criteria.list();
//		return result;
		
		Session session1 = sessionFactory.getCurrentSession();
		List<RoleModel> result1 = session1.createQuery("from RoleModel where (code='"+keySearch+"'"
				+ " or name='"+keyName+"'"
				+ " or TRUNC(createdDate) = TO_DATE('"+createdDate+"', 'yyyy-MM-dd')"
				+ " or createdBy = '"+keyCreatedBy+"') and isDelete = 0").list();
		
		return result1;
		
	}


	@Override
	public RoleModel getById(Integer id) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		RoleModel result = session.get(RoleModel.class, id);
		return result;
	}

	@Override
	public void insert(RoleModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(model);
	}

	@Override
	public void update(RoleModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(model);
	}

	@Override
	public void delete(RoleModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(model);
	}
}

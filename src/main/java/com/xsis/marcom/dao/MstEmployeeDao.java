package com.xsis.marcom.dao;

import java.util.List;


import com.xsis.marcom.model.MstEmployeeModel;

public interface MstEmployeeDao {
	public Boolean checkCode (String code) throws Exception;
	public List<MstEmployeeModel> get() throws Exception;
	public List<MstEmployeeModel> search(String keySearch, String keyfirstName, String company, String createdDate, String keyCreatedBy) throws Exception;
	public MstEmployeeModel getById(int id) throws Exception;
	public void insert(MstEmployeeModel model) throws Exception;
	public void update(MstEmployeeModel model) throws Exception;
	public void delete(MstEmployeeModel model) throws Exception;

}

package com.xsis.marcom.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.marcom.common.StringConverter;
import com.xsis.marcom.dao.MstProductDao;
import com.xsis.marcom.model.MstEmployeeModel;
import com.xsis.marcom.model.MstProductModel;

@Repository
public class MstProductDaoImpl implements MstProductDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public String getCode() throws Exception {
		String result = "";
		Session session = sessionFactory.getCurrentSession();
		List list = session.createQuery("select max(r.code) from MstProductModel r").list();
		result = list.get(0).toString();
		if(result.equals("")) {
			result = "PR0001";
		} else {
			String[] arrCode = result.split("R");
			int newCode = Integer.parseInt(arrCode[1]) + 1;
			result = "PR" + StringConverter.inToString(newCode, 4);
		}
		return result;
	}

	@Override
	public List<MstProductModel> get() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		List<MstProductModel> result = session.createQuery("from MstProductModel where isDelete = 0").list();
		return result;
	}

	@Override
	public List<MstProductModel> search(String keySearch, String keyName, String keyDescrip, String createdDate, String keyCreatedBy) throws Exception {
		/* Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(MstProductModel.class);
		criteria.add(Restrictions.like("name", "%" + keySearch + "%"));
		List<MstProductModel> result = criteria.list();
		return result; */
		
		Session session = sessionFactory.getCurrentSession();
		List<MstProductModel> result = session.createQuery("from MstProductModel where (code = '"+keySearch+"'"
				+ " or name = '"+keyName+"'"
				+ " or description = '"+keyDescrip+"'"
				+ " or TRUNC(createdDate) = TO_DATE('"+createdDate+"', 'yyyy-MM-dd')"
				+ " or createdBy = '"+keyCreatedBy+"') and isDelete=0").list();
		
		return result;
	}

	@Override
	public MstProductModel getById(int id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		MstProductModel result = session.get(MstProductModel.class, id);
		return result;
	}

	@Override
	public void insert(MstProductModel model) throws Exception {
		sessionFactory.getCurrentSession().save(model);
	}

	@Override
	public void update(MstProductModel model) throws Exception {
		sessionFactory.getCurrentSession().update(model);
	}

	@Override
	public void delete(MstProductModel model) throws Exception {
		sessionFactory.getCurrentSession().delete(model);
	}

	@Override
	public Boolean checkName(String name) throws Exception {
		Boolean result = false;
		Session session = sessionFactory.getCurrentSession();
		
		Criteria criteria = session.createCriteria(MstProductModel.class);
		criteria.add(Restrictions.like("name", "%" + name + "%"));
		List<MstProductModel> list = criteria.list();
		if(list.size() > 0) {
			result = true;
		}
		return result;
	}

}

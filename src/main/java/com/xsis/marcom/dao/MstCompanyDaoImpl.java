package com.xsis.marcom.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.marcom.common.StringConverter;
import com.xsis.marcom.dao.MstCompnyDao;
import com.xsis.marcom.model.MstCompanyModel;

@Repository
public class MstCompanyDaoImpl implements MstCompnyDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public String getCode() throws Exception {
		String result = "";
		Session session = sessionFactory.getCurrentSession();
		List list = session.createQuery("select max(r.code) from MstCompanyModel r").list();
		result = list.get(0).toString();
		if(result.equals("")) {
			result = "CP0001";
		} else {
			String[] arrCode = result.split("P");
			int newCode = Integer.parseInt(arrCode[1]) + 1;
			result = "CP" + StringConverter.inToString(newCode, 4);
		}
		return result;
	}
	
	@Override
	public List<MstCompanyModel> get() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		List<MstCompanyModel> result = session.createQuery("from MstCompanyModel where isDelete = 0").list();
		return result;
	}

	@Override
	public List<MstCompanyModel> search(String keySearch, String keyName, String createdDate, String keyCreatedBy) throws Exception {		
		/* Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(MstCompanyModel.class);
		criteria.add(Restrictions.like("name", "%" + keySearch + "%"));
		List<MstCompanyModel> result = criteria.list();
		return result; */
		
		Session session = sessionFactory.getCurrentSession();
		List<MstCompanyModel> result = session.createQuery("from MstCompanyModel where (code='"+keySearch+"'"
				+ " or name='"+keyName+"'"
				+ " or TRUNC(createdDate) = TO_DATE('"+createdDate+"', 'yyyy-MM-dd')"
				+ " or createdBy ='"+keyCreatedBy+"') and isDelete=0").list();
		
		return result;
		
	}

	@Override
	public MstCompanyModel getById(int id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		MstCompanyModel result = session.get(MstCompanyModel.class, id);
		return result;
	}

	@Override
	public void insert(MstCompanyModel model) throws Exception {
		sessionFactory.getCurrentSession().save(model);
	}

	@Override
	public void update(MstCompanyModel model) throws Exception {
		sessionFactory.getCurrentSession().update(model);
	}

	@Override
	public void delete(MstCompanyModel model) throws Exception {
		model.setIsDelete(1);
		sessionFactory.getCurrentSession().update(model);
	}

	@Override
	public Boolean checkName(String name) throws Exception {
		Boolean result = false;
		Session session = sessionFactory.getCurrentSession();
		
		List<MstCompanyModel> list = session.createQuery("from MstCompanyModel where name='"+name+"' and isDelete = 0").list();
		Criteria criteria = session.createCriteria(MstCompanyModel.class);
		criteria.add(Restrictions.like("name","%" + name + "%"));
		//List<MstCompanyModel> list = criteria.list();
		if(list.size() > 0) {
			result = true;
		}
		return result;
	}


}

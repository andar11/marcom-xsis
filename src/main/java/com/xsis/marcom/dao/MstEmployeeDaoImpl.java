package com.xsis.marcom.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.marcom.dao.MstEmployeeDao;
import com.xsis.marcom.model.MstEmployeeModel;

@Repository
public class MstEmployeeDaoImpl implements MstEmployeeDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<MstEmployeeModel> get() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		List<MstEmployeeModel> result = session.createQuery("from MstEmployeeModel where isDelete = 0").list();
		return result;
	}

	@Override
	public List<MstEmployeeModel> search(String keySearch, String keyfirstName, String company, String createdDate, String keyCreatedBy) throws Exception {		
		/* Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(MstEmployeeModel.class);
		criteria.add(Restrictions.like("name", "%" + keySearch + "%"));
		List<MstEmployeeModel> result = criteria.list();
		return result; */
		
		Session session = sessionFactory.getCurrentSession();
		List<MstEmployeeModel> result = session.createQuery("from MstEmployeeModel where (code = '"+keySearch+"'"
				+ " or firstName = '"+keyfirstName+"'"
				+ " or mCompanyId='"+company+"'"
				+ " or TRUNC(createdDate) = TO_DATE('"+createdDate+"', 'yyyy-MM-dd')"
				+ " or createdBy = '"+keyCreatedBy+"') and isDelete = 0 ").list();
		return result;
	}

	@Override
	public MstEmployeeModel getById(int id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		MstEmployeeModel result = session.get(MstEmployeeModel.class, id);
		return result;
	}

	@Override
	public void insert(MstEmployeeModel model) throws Exception {
		sessionFactory.getCurrentSession().save(model);
	}

	@Override
	public void update(MstEmployeeModel model) throws Exception {
		sessionFactory.getCurrentSession().update(model);
	}

	@Override
	public void delete(MstEmployeeModel model) throws Exception {
		sessionFactory.getCurrentSession().delete(model);
	}

	@Override
	public Boolean checkCode(String code) throws Exception {
		Boolean result = false;
		Session session = sessionFactory.getCurrentSession();
		
		Criteria criteria = session.createCriteria(MstEmployeeModel.class);
		criteria.add(Restrictions.like("code", "%" + code + "%"));
		List<MstEmployeeModel> list = criteria.list();
		if(list.size() > 0) {
			result = true;
		}
		return result;
	}

}

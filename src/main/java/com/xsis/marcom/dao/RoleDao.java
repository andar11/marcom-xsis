package com.xsis.marcom.dao;

import java.util.List;

import com.xsis.marcom.model.RoleModel;

public interface RoleDao {

	public String getCode() throws Exception;
	public List<RoleModel> get() throws Exception;
	public List<RoleModel> search(String keySearch,String keyName,String createdDate,String keyCreatedBy) throws Exception;
	public RoleModel getById(Integer id) throws Exception;
	public void insert(RoleModel model) throws Exception;
	public void update(RoleModel model) throws Exception;
	public void delete(RoleModel model) throws Exception;

}
